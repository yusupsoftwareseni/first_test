require "rails_helper"
require 'spec_helper'

RSpec.describe PopularsController, :type => :controller do
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe "GET #detail_artist" do
    it "responds successfully with an HTTP 200 status code" do
      get :detail_artist
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the detail_artist template" do
      get :detail_artist
      expect(response).to render_template("detail_artist")
    end

  end
end