class PopularsController < ApplicationController

  require 'rest-client'

  def index
    keyword = params['keyword'].present? ? params['keyword'].gsub(/\s+/, '+') : 'Indonesia'
    @keyword = params['keyword'].present? ? params['keyword'].gsub(/\s+/, '+') : nil
    @page = params['page'].present? ? params['page'].to_i : 1

    # Get result from API last.fm
    results = JSON.parse(RestClient.get("http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=#{keyword}&api_key=385abba2af409c4db2a8829bcaec948a&limit=5&page=#{@page}&format=json"))
    if results["error"].present?
      flash[:alert] = "country name invalid or not listed"
      redirect_to root_path
    else
      @top_artist = results["topartists"]["artist"]
      @values = WillPaginate::Collection.create(@page, 5,  results["topartists"]["@attr"]["totalPages"]) do |pager|
        pager.replace @top_artist
      end
    end
  end

  def detail_artist
    @name = params["name"]
    # Get result from API last.fm
    results = JSON.parse(RestClient.get("http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=#{@name}&api_key=385abba2af409c4db2a8829bcaec948a&format=json"))
    @top_tracks = results["toptracks"]["track"]
  end
end
